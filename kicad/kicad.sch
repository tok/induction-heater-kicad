EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Induction Heater Driver"
Date "2019-08-30"
Rev "v0.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:L L?
U 1 1 5D69A275
P 9900 4050
F 0 "L?" H 9953 4096 50  0000 L CNN
F 1 "L" H 9953 4005 50  0000 L CNN
F 2 "" H 9900 4050 50  0001 C CNN
F 3 "~" H 9900 4050 50  0001 C CNN
	1    9900 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L?
U 1 1 5D69A77A
P 9900 4350
F 0 "L?" H 9953 4396 50  0000 L CNN
F 1 "L" H 9953 4305 50  0000 L CNN
F 2 "" H 9900 4350 50  0001 C CNN
F 3 "~" H 9900 4350 50  0001 C CNN
	1    9900 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D69AFFF
P 9500 4200
F 0 "C?" H 9615 4246 50  0000 L CNN
F 1 "2.2u" H 9615 4155 50  0000 L CNN
F 2 "" H 9538 4050 50  0001 C CNN
F 3 "~" H 9500 4200 50  0001 C CNN
	1    9500 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 4050 9500 3900
Wire Wire Line
	9500 3900 9900 3900
Wire Wire Line
	9500 4350 9500 4500
Wire Wire Line
	9500 4500 9900 4500
Wire Notes Line
	9150 3700 9150 4700
Wire Notes Line
	9150 4700 10150 4700
Wire Notes Line
	10150 4700 10150 3700
Wire Notes Line
	10150 3700 9150 3700
Text Notes 9500 3650 0    50   ~ 0
LC Tank
$Comp
L Driver_FET:MCP14A0304xMNY U1
U 1 1 5D69D9AD
P 4850 4200
F 0 "U1" H 4700 4600 50  0000 C CNN
F 1 "MCP 14E10" H 5250 4600 50  0000 C CNN
F 2 "Package_DFN_QFN:WDFN-8-1EP_3x2mm_P0.5mm_EP1.3x1.4mm" H 4850 4950 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/MCP14A0303_4_5-Data-Sheet-20006046A.pdf" H 4850 3900 50  0001 C CNN
	1    4850 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_DGS Q1
U 1 1 5D69F39B
P 6150 3900
F 0 "Q1" H 6356 3946 50  0000 L CNN
F 1 "NMOS (IRFP 4332)" H 6356 3855 50  0000 L CNN
F 2 "" H 6350 4000 50  0001 C CNN
F 3 "~" H 6150 3900 50  0001 C CNN
	1    6150 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_DGS Q2
U 1 1 5D6A0740
P 6150 4500
F 0 "Q2" H 6356 4454 50  0000 L CNN
F 1 "NMOS (IRFP 4332)" H 6356 4545 50  0000 L CNN
F 2 "" H 6350 4600 50  0001 C CNN
F 3 "~" H 6150 4500 50  0001 C CNN
	1    6150 4500
	1    0    0    1   
$EndComp
Wire Wire Line
	9000 3900 9500 3900
Connection ~ 9500 3900
Wire Wire Line
	9000 4500 9500 4500
Connection ~ 9500 4500
Wire Wire Line
	9900 4200 10100 4200
Wire Wire Line
	10100 4200 10100 3750
Wire Wire Line
	10100 3750 9000 3750
Connection ~ 9900 4200
Wire Wire Line
	6250 4100 6250 4200
Wire Wire Line
	6250 4200 6700 4200
Connection ~ 6250 4200
Wire Wire Line
	6250 4200 6250 4300
$Comp
L power:GND #PWR?
U 1 1 5D6A9EB1
P 6700 4200
F 0 "#PWR?" H 6700 3950 50  0001 C CNN
F 1 "GND" H 6705 4027 50  0000 C CNN
F 2 "" H 6700 4200 50  0001 C CNN
F 3 "" H 6700 4200 50  0001 C CNN
	1    6700 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D6AA04B
P 4850 4650
F 0 "#PWR?" H 4850 4400 50  0001 C CNN
F 1 "GND" H 4855 4477 50  0000 C CNN
F 2 "" H 4850 4650 50  0001 C CNN
F 3 "" H 4850 4650 50  0001 C CNN
	1    4850 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 4600 4850 4650
Wire Wire Line
	5250 4100 5700 4100
Wire Wire Line
	5700 4100 5700 3900
Wire Wire Line
	5700 3900 5950 3900
Wire Wire Line
	5250 4300 5700 4300
Wire Wire Line
	5700 4300 5700 4500
Wire Wire Line
	5700 4500 5950 4500
NoConn ~ 4450 4000
NoConn ~ 4450 4400
Wire Wire Line
	6250 5100 7250 5100
Wire Wire Line
	7250 4500 7400 4500
Wire Wire Line
	6250 3300 7250 3300
$Comp
L Connector:Conn_01x01_Female J?
U 1 1 5D6BD6C9
P 7600 4500
F 0 "J?" H 7628 4526 50  0000 L CNN
F 1 "Conn_01x01_Female" H 7628 4435 50  0000 L CNN
F 2 "" H 7600 4500 50  0001 C CNN
F 3 "~" H 7600 4500 50  0001 C CNN
	1    7600 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 3900 7400 3900
$Comp
L Connector:Conn_01x01_Female J?
U 1 1 5D6BCCC0
P 7600 3900
F 0 "J?" H 7628 3926 50  0000 L CNN
F 1 "Conn_01x01_Female" H 7628 3835 50  0000 L CNN
F 2 "" H 7600 3900 50  0001 C CNN
F 3 "~" H 7600 3900 50  0001 C CNN
	1    7600 3900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J?
U 1 1 5D6C47BC
P 8800 3900
F 0 "J?" H 8908 4081 50  0000 C CNN
F 1 "Conn_01x01_Male" H 8908 3990 50  0000 C CNN
F 2 "" H 8800 3900 50  0001 C CNN
F 3 "~" H 8800 3900 50  0001 C CNN
	1    8800 3900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J?
U 1 1 5D6C5956
P 8800 4500
F 0 "J?" H 8908 4681 50  0000 C CNN
F 1 "Conn_01x01_Male" H 8908 4590 50  0000 C CNN
F 2 "" H 8800 4500 50  0001 C CNN
F 3 "~" H 8800 4500 50  0001 C CNN
	1    8800 4500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J?
U 1 1 5D6C5F39
P 8800 3750
F 0 "J?" H 8908 3931 50  0000 C CNN
F 1 "Conn_01x01_Male" H 8908 3840 50  0000 C CNN
F 2 "" H 8800 3750 50  0001 C CNN
F 3 "~" H 8800 3750 50  0001 C CNN
	1    8800 3750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J?
U 1 1 5D6C63C0
P 7600 3750
F 0 "J?" H 7628 3776 50  0000 L CNN
F 1 "Conn_01x01_Female" H 7628 3685 50  0000 L CNN
F 2 "" H 7600 3750 50  0001 C CNN
F 3 "~" H 7600 3750 50  0001 C CNN
	1    7600 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:L_Core_Ferrite L?
U 1 1 5D6D2494
P 5100 2650
F 0 "L?" V 4919 2650 50  0000 C CNN
F 1 "47-200uH" V 5010 2650 50  0000 C CNN
F 2 "" H 5100 2650 50  0001 C CNN
F 3 "~" H 5100 2650 50  0001 C CNN
	1    5100 2650
	0    1    1    0   
$EndComp
Wire Wire Line
	5250 2650 7400 2650
Wire Wire Line
	7400 2650 7400 3750
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5D6D5283
P 1050 1000
F 0 "#FLG?" H 1050 1075 50  0001 C CNN
F 1 "PWR_FLAG" H 1050 1173 50  0000 C CNN
F 2 "" H 1050 1000 50  0001 C CNN
F 3 "~" H 1050 1000 50  0001 C CNN
	1    1050 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D6D5FBB
P 1050 1100
F 0 "#PWR?" H 1050 850 50  0001 C CNN
F 1 "GND" H 1055 927 50  0000 C CNN
F 2 "" H 1050 1100 50  0001 C CNN
F 3 "" H 1050 1100 50  0001 C CNN
	1    1050 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 1100 1050 1000
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5D6D6B04
P 1550 1100
F 0 "#FLG?" H 1550 1175 50  0001 C CNN
F 1 "PWR_FLAG" H 1550 1273 50  0000 C CNN
F 2 "" H 1550 1100 50  0001 C CNN
F 3 "~" H 1550 1100 50  0001 C CNN
	1    1550 1100
	-1   0    0    1   
$EndComp
$Comp
L power:+24V #PWR?
U 1 1 5D6D95A4
P 4200 2550
F 0 "#PWR?" H 4200 2400 50  0001 C CNN
F 1 "+24V" H 4215 2723 50  0000 C CNN
F 2 "" H 4200 2550 50  0001 C CNN
F 3 "" H 4200 2550 50  0001 C CNN
	1    4200 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2550 4200 2650
Wire Wire Line
	4200 2650 4950 2650
$Comp
L power:+24V #PWR?
U 1 1 5D6DA734
P 1550 1000
F 0 "#PWR?" H 1550 850 50  0001 C CNN
F 1 "+24V" H 1565 1173 50  0000 C CNN
F 2 "" H 1550 1000 50  0001 C CNN
F 3 "" H 1550 1000 50  0001 C CNN
	1    1550 1000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1550 1100 1550 1000
$Comp
L Device:D D?
U 1 1 5D6E8FB0
P 3700 3300
F 0 "D?" H 3700 3516 50  0000 C CNN
F 1 "400V Fast" H 3700 3425 50  0000 C CNN
F 2 "" H 3700 3300 50  0001 C CNN
F 3 "~" H 3700 3300 50  0001 C CNN
	1    3700 3300
	-1   0    0    -1  
$EndComp
$Comp
L Device:D D?
U 1 1 5D6EA34E
P 3700 5100
F 0 "D?" H 3700 5316 50  0000 C CNN
F 1 "400V Fast" H 3700 5225 50  0000 C CNN
F 2 "" H 3700 5100 50  0001 C CNN
F 3 "~" H 3700 5100 50  0001 C CNN
	1    3700 5100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3850 5100 6250 5100
Connection ~ 6250 5100
Wire Wire Line
	3850 3300 6250 3300
Connection ~ 6250 3300
$Comp
L Device:D_Zener D?
U 1 1 5D6F9EDE
P 3200 3950
F 0 "D?" V 3154 4029 50  0000 L CNN
F 1 "12V" V 3245 4029 50  0000 L CNN
F 2 "" H 3200 3950 50  0001 C CNN
F 3 "~" H 3200 3950 50  0001 C CNN
	1    3200 3950
	0    1    1    0   
$EndComp
$Comp
L Device:D_Zener D?
U 1 1 5D6FC119
P 3200 4450
F 0 "D?" V 3246 4371 50  0000 R CNN
F 1 "12V" V 3155 4371 50  0000 R CNN
F 2 "" H 3200 4450 50  0001 C CNN
F 3 "~" H 3200 4450 50  0001 C CNN
	1    3200 4450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 4700 6250 5100
Wire Wire Line
	7250 4500 7250 5100
Wire Wire Line
	7250 3300 7250 3900
Wire Wire Line
	6250 3300 6250 3700
Wire Wire Line
	3200 4300 3200 4200
Wire Wire Line
	3200 4200 2400 4200
Wire Wire Line
	2400 4200 2400 4250
Connection ~ 3200 4200
Wire Wire Line
	3200 4200 3200 4100
$Comp
L power:GND #PWR?
U 1 1 5D72B55A
P 2400 4250
F 0 "#PWR?" H 2400 4000 50  0001 C CNN
F 1 "GND" H 2405 4077 50  0000 C CNN
F 2 "" H 2400 4250 50  0001 C CNN
F 3 "" H 2400 4250 50  0001 C CNN
	1    2400 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 4200 3600 4200
$Comp
L Device:R R?
U 1 1 5D72C9E1
P 3600 3950
F 0 "R?" H 3670 3996 50  0000 L CNN
F 1 "10k" H 3670 3905 50  0000 L CNN
F 2 "" V 3530 3950 50  0001 C CNN
F 3 "~" H 3600 3950 50  0001 C CNN
	1    3600 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D72D588
P 3600 4450
F 0 "R?" H 3670 4496 50  0000 L CNN
F 1 "10k" H 3670 4405 50  0000 L CNN
F 2 "" V 3530 4450 50  0001 C CNN
F 3 "~" H 3600 4450 50  0001 C CNN
	1    3600 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 3800 3600 3800
Wire Wire Line
	3600 4100 3600 4200
Connection ~ 3600 4200
Wire Wire Line
	3600 4300 3600 4200
Wire Wire Line
	3600 4600 3200 4600
Wire Wire Line
	3200 3800 2750 3800
Connection ~ 3200 3800
Wire Wire Line
	2750 5100 3550 5100
Connection ~ 3200 4600
Wire Wire Line
	3600 3800 4150 3800
Wire Wire Line
	4150 3800 4150 4100
Wire Wire Line
	4150 4100 4450 4100
Connection ~ 3600 3800
Wire Wire Line
	3600 4600 4150 4600
Wire Wire Line
	4150 4600 4150 4300
Wire Wire Line
	4150 4300 4450 4300
Connection ~ 3600 4600
Wire Wire Line
	2550 3300 2550 4600
Wire Wire Line
	2550 3300 3550 3300
Wire Wire Line
	2550 4600 3200 4600
Wire Wire Line
	2750 5100 2750 3800
Connection ~ 2750 3800
Connection ~ 2550 4600
$Comp
L Device:R R?
U 1 1 5D73F279
P 2000 3800
F 0 "R?" V 2207 3800 50  0000 C CNN
F 1 "470R 2W" V 2116 3800 50  0000 C CNN
F 2 "" V 1930 3800 50  0001 C CNN
F 3 "~" H 2000 3800 50  0001 C CNN
	1    2000 3800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D73F9B8
P 2000 4600
F 0 "R?" V 1793 4600 50  0000 C CNN
F 1 "470R 2W" V 1884 4600 50  0000 C CNN
F 2 "" V 1930 4600 50  0001 C CNN
F 3 "~" H 2000 4600 50  0001 C CNN
	1    2000 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	2150 3800 2750 3800
Wire Wire Line
	2150 4600 2550 4600
Wire Wire Line
	1850 4600 1700 4600
Wire Wire Line
	1700 4600 1700 3800
Wire Wire Line
	1700 3800 1850 3800
Wire Wire Line
	1700 3150 1700 3800
Connection ~ 1700 3800
$Comp
L power:+12V #PWR?
U 1 1 5D759989
P 4850 3650
F 0 "#PWR?" H 4850 3500 50  0001 C CNN
F 1 "+12V" H 4865 3823 50  0000 C CNN
F 2 "" H 4850 3650 50  0001 C CNN
F 3 "" H 4850 3650 50  0001 C CNN
	1    4850 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 3650 4850 3800
$Comp
L power:+12V #PWR?
U 1 1 5D76D888
P 2050 1000
F 0 "#PWR?" H 2050 850 50  0001 C CNN
F 1 "+12V" H 2065 1173 50  0000 C CNN
F 2 "" H 2050 1000 50  0001 C CNN
F 3 "" H 2050 1000 50  0001 C CNN
	1    2050 1000
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5D76DC53
P 2050 1100
F 0 "#FLG?" H 2050 1175 50  0001 C CNN
F 1 "PWR_FLAG" H 2050 1273 50  0000 C CNN
F 2 "" H 2050 1100 50  0001 C CNN
F 3 "~" H 2050 1100 50  0001 C CNN
	1    2050 1100
	-1   0    0    1   
$EndComp
Wire Wire Line
	2050 1100 2050 1000
$Comp
L power:+12V #PWR?
U 1 1 5D7E4589
P 1700 3150
F 0 "#PWR?" H 1700 3000 50  0001 C CNN
F 1 "+12V" H 1715 3323 50  0000 C CNN
F 2 "" H 1700 3150 50  0001 C CNN
F 3 "" H 1700 3150 50  0001 C CNN
	1    1700 3150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
